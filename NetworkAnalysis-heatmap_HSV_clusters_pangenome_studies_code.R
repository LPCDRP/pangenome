
library(ComplexHeatmap)
library(tidyverse)

#sv expression p/a
svs<-read.table(file = "struct_presence_absence.Rtab",header = TRUE,check.names = FALSE)


a<-read.csv("cluster4.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
b<-read.csv("cluster8.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
c<-read.csv("cluster9.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
d<-read.csv("cluster12.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
e<-read.csv("cluster14.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
f<-read.csv("cluster16.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
g<-read.csv("cluster17.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
h<-read.csv("cluster18.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
i<-read.csv("cluster19.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
j<-read.csv("cluster20.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)
k<-read.csv("cluster25.gene.pa.csv",header=TRUE,check.names = FALSE,row.names = 1)

# matrix for all the genes 
all.clusters<-as.data.frame(rbind(a,b,c,d,e,f,g,h,i,j,k))
all.clusters.mat<-as.matrix(all.clusters)

# metadataframe for all the clusters to make heatmap
clusters<-as.data.frame(cbind(c(rownames(a),rownames(b),rownames(c),rownames(d),rownames(e),rownames(f),rownames(g),rownames(h),rownames(i),rownames(j),rownames(k))))

clusters$cluster[clusters$V1%in%row.names(a)]<-"A"
clusters$cluster[clusters$V1%in%row.names(b)]<-"B"
clusters$cluster[clusters$V1%in%row.names(c)]<-"C"
clusters$cluster[clusters$V1%in%row.names(d)]<-"D"
clusters$cluster[clusters$V1%in%row.names(e)]<-"E"
clusters$cluster[clusters$V1%in%row.names(f)]<-"F"
clusters$cluster[clusters$V1%in%row.names(g)]<-"G"
clusters$cluster[clusters$V1%in%row.names(h)]<-"H"
clusters$cluster[clusters$V1%in%row.names(i)]<-"I"
clusters$cluster[clusters$V1%in%row.names(j)]<-"J"
clusters$cluster[clusters$V1%in%row.names(k)]<-"K"


# df with all lineage and drug resistance information
master.phen<-read.csv(file = "master.phenotypes.csv",header = TRUE,row.names = 1)

# create annotation dataframe


mp=as.data.frame(master.phen$lineage)
colnames(mp)<-"lineage"

mp$lineage<-recode(mp$lineage,lineage7="L7")

# column annotations dataframe

col_annot=HeatmapAnnotation(df = mp,which="column",
          col=list(lineage=c("L1"="plum1","L2"='blue4',"L3"='mediumpurple3',
                        "L4"='red1',"L5"='coral4',"L6"='seagreen',"L7"='yellow')))
clust<-as.data.frame(clusters$cluster)
colnames(clust)<-"cluster"
# row annotations dataframe
row_annot=HeatmapAnnotation(df = clust,which="row",
                            col=list(cluster=c("A"="deeppink3","B"='darkgoldenrod',"C"='gold4',
                                               "D"='sienna',"E"='darkorange',"F"='olivedrab',"G"='firebrick4',
                                               "H"='coral1',"I"='burlywood3',"J"='bisque3',"K"='indianred3')))

# heatmap with grouping by categorical variables (i.e. lineage)


heat<-Heatmap(matrix = all.clusters.mat,column_title = "Isolates",row_title = "Genes in Highly Structurally Variant Regions",
           top_annotation = col_annot,right_annotation = row_annot,
           column_split = mp$lineage, # split heatmap by lineage
            row_split = clusters$cluster,cluster_row_slices = TRUE,
           cluster_columns = TRUE,clustering_distance_columns = "binary",clustering_method_columns = "complete",#column clustering
        cluster_rows = TRUE,clustering_distance_rows = "binary",clustering_method_rows = "complete",
          show_heatmap_legend = FALSE,
           column_names_gp = gpar(fontsize=7),column_names_rot = 45,
           col = RColorBrewer::brewer.pal(name = "Blues", n = 7),column_gap = unit(2,"mm"),
           rect_gp = gpar(col = "black", lwd = 0.000002),raster_quality = 600)

heat
color_mapping_legend()
heat=draw(heat)
rorder<-row_order(heat)


# heatmap without grouping by categorical variables (i.e. lineage)


norv<-read.csv(file = "lineage4.svs.noh37Rvincluded.csv",header = FALSE)
rv<-read.csv(file = "lineage4.svs.h37Rvincluded.csv",header = FALSE)

diff<-as.data.frame(rv$V1[!(rv$V1%in%norv$V1)])
write.csv(x = diff,file = "struc.variants.when.noRv.csv")

# Plot of pangenome studies 

df<-c("Our Study",110,3767)
pan2<-rbind(pan,df)

pan2$Sample.Size <- as.numeric(as.character(pan2$Sample.Size)) 
pan2$Core.Genome.Size <- as.numeric(as.character(pan2$Core.Genome.Size)) 

ggplot(data = pan2,aes(x=Sample.Size,y=Core.Genome.Size))+
  geom_point(color="dodgerblue")+xlim(0,4500)+geom_text(label=pan2$Author,nudge_x = 120)+theme_bw()

