# import text file with shared name column of all genes found in network 
# import list of genes involved in structural variants only found in that region 
# create a dataframe and save a text file to import into cytoscape 

all_nodes<-read.csv(file = "final_graph_with_ref_dissimilar.gml default node.csv",header = TRUE)
lineage1.specific.svs.genes<-read.csv(file = "lineage1.specific.svs.genes.csv",header = FALSE, check.names = FALSE)

#lineage 1
lineage1.table<-as.data.frame(all_nodes$shared.name) # new dataframe with all nodes in network 
colnames(lineage1.table)<-c("shared name","L1Specific")

lineage1.table$specific[lineage1.table$`all_nodes$shared.name` %in% lineage1.specific.svs.genes$V1]<-1 #L1 specific variable column
lineage1.table$specific[!(lineage1.table$`all_nodes$shared.name` %in% lineage1.specific.svs.genes$V1)]<-0

write.table(x = lineage1.table,"lineage1.table.txt",sep = "\t",quote = FALSE,row.names = FALSE)

#lineage2
lineage2.specific.svs.genes<-read.csv(file = "lineage2.specific.svs.genes.csv",header = FALSE, check.names = FALSE)

lineage2.table<-as.data.frame(all_nodes$shared.name) # new dataframe with all nodes in network 
colnames(lineage2.table)<-c("shared name","L2Specific")

lineage2.table$specific[lineage2.table$`all_nodes$shared.name` %in% lineage2.specific.svs.genes$V1]<-1 #L2 specific variable column
lineage2.table$specific[!(lineage2.table$`all_nodes$shared.name` %in% lineage2.specific.svs.genes$V1)]<-0

write.table(x = lineage2.table,"lineage2.table.txt",sep = "\t",quote = FALSE,row.names = FALSE)
colnames(lineage2.table)<-c("shared name","L2Specific")

#lineage3
lineage3.specific.svs.genes<-read.csv(file = "lineage3.specific.svs.genes.csv",header = FALSE, check.names = FALSE)

lineage3.table<-as.data.frame(all_nodes$shared.name) # new dataframe with all nodes in network 


lineage3.table$specific[lineage3.table$`all_nodes$shared.name` %in% lineage3.specific.svs.genes$V1]<-1 #L3 specific variable column
lineage3.table$specific[!(lineage3.table$`all_nodes$shared.name` %in% lineage3.specific.svs.genes$V1)]<-0

colnames(lineage3.table)<-c("shared name","L3Specific")
write.table(x = lineage3.table,"lineage3.table.txt",sep = "\t",quote = FALSE,row.names = FALSE)

#lineage4 without H37Rv
lineage4.specific.svs.genes<-read.csv(file = "lineage4.specific.svs_noh37rv.genes.csv",header = FALSE, check.names = FALSE)

lineage4.table<-as.data.frame(all_nodes$shared.name) # new dataframe with all nodes in network 


lineage4.table$specific[lineage4.table$`all_nodes$shared.name` %in% lineage4.specific.svs.genes$V1]<-1 #L4 specific variable column
lineage4.table$specific[!(lineage4.table$`all_nodes$shared.name` %in% lineage4.specific.svs.genes$V1)]<-0

colnames(lineage4.table)<-c("shared name","L4Specific_noH37Rv")
write.table(x = lineage4.table,"lineage4.table.noH37Rv.txt",sep = "\t",quote = FALSE,row.names = FALSE)

#lineage4 with H37Rv
lineage4.specific.svs.genes<-read.csv(file = "lineage4.svs.h37Rvincluded_genes.csv",header = FALSE, check.names = FALSE)

lineage4.table<-as.data.frame(all_nodes$shared.name) # new dataframe with all nodes in network 


lineage4.table$specific[lineage4.table$`all_nodes$shared.name` %in% lineage4.specific.svs.genes$V1]<-1 #L4+H37Rv specific variable column
lineage4.table$specific[!(lineage4.table$`all_nodes$shared.name` %in% lineage4.specific.svs.genes$V1)]<-0

colnames(lineage4.table)<-c("shared name","L4Specific_H37Rv")
write.table(x = lineage4.table,"lineage4.table.H37Rv.txt",sep = "\t",quote = FALSE,row.names = FALSE)
