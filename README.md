# Scripts associated with a pangenome analysis using Roary

Pipeline and scripts associated with the analysis of a pangenome using Roary[1]
To use these scripts, [AnnoTUB](https://gitlab.com/LPCDRP/annotub) is the recommended annotation pipeline.

### Dependencies and Usages
See README.md within each subdirectory for usage details

[1]Page, A. J. et al. Roary: Rapid large-scale prokaryote pan genome analysis. Bioinformatics 1–3 (2015). doi:10.1093/bioinformatics/btv421

